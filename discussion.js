//=================================AGGREGATION
const database = 'test';
use(database);

db.test.insertMany([
	{
		name: "Apple",
		color: "Red",
		stock: 20,
		price: 40,
		supplier_id: 1,
		onSale: true,
		origin: ["Philippines", "US"]
	},
	{
		name: "Banana",
		color: "Yellow",
		stock: 15,
		price: 20,
		supplier_id: 2,
		onSale: true,
		origin: ["Philippines", "Ecuador"]
	},
	{
		name: "Kiwi",
		color: "Green",
		stock: 25,
		price: 50,
		supplier_id: 1,
		onSale: true,
		origin: ["China", "US"]
	},
	{
		name: "Mango",
		color: "Yellow",
		stock: 10,
		price: 120,
		supplier_id: 2,
		onSale: false,
		origin: ["Philippines", "India"]
	}
]);

//===============================($match) & ($group)
/*
	$match: {field: value}
	$group: {_id: value, fieldResult: "valueResult"}
*/


db.test.aggregate([
	{$match: {onSale: true}},
	{$group: {_id: "$supplier_id", total: {$sum: "$stock"}}}
]);

//===============================($project)

db.test.aggregate([
	{$group:{_id: null, onSale: true, total: {$sum: "$stock"}}}
]);

//=================================($sort)
db.test.aggregate([
	{$match:{onSale: true}},
	{$group:{_id: "$supplier_id", total: {$sum: 1}}},
	{$sort: {_total: -1}} // sort total in reverse
]);

//============================($unwind)
db.test.aggregate([
	{$unwind: "$origin" }// deconstruct depends on country of origin
]);

db.test.aggregate([
	{$unwind: "$origin" },
	{$group: {_id: "$name", kinds:{$sum:1}}}
]);


